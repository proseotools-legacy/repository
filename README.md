Maven Repository
================

Maven repository of publicly available artifacts, either made or used by Pro SEO Tools.

## Usage

### Maven

Add the following code to the project's POM

```
<repositories>
    <repository>
        <id>proseotools-snapshots</id>
        <url>https://raw.github.com/proseotools/repository/master/snapshots/</url>
        <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
        </snapshots>
    </repository>
    <repository>
        <id>proseotools-releases</id>
        <url>https://raw.github.com/proseotools/repository/master/releases/</url>
    </repository>
</repositories>
```

### Grails

Add the following code to the `repositories` section of `BuildConfig.groovy`

```
    repositories {
        mavenRepo "https://raw.github.com/proseotools/repository/master/releases/"
        mavenRepo "https://raw.github.com/proseotools/repository/master/snapshots/"
    }
```
